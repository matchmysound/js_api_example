import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { MatchMySound } from 'matchmysound';


var mms_init_done = false;

var etalon_url = 'https://mms-general.s3.amazonaws.com:443/751/65062d43-e28b-401a-a6eb-4ddc402caeb0.eta';
var beg = 0; // match to the etalon from the beginning
var end = 20; // and match until 20 seconds in etalon
var sync = 1; // 0 means "follow me", 1 means metronome mode matching
var tempo = 100; // tempo in percentage
var match_start = 2.0; // match start time in seconds


class App extends Component {
  constructor(props: any) {
    super(props);
    if (!mms_init_done) {
      MatchMySound.init().then(() => {
        MatchMySound.load_etalon(etalon_url);
      });
      mms_init_done = true;
    }
  }

  run_matching() {
    MatchMySound.start_matching(beg, end, sync, tempo, match_start).then((data: any) => {
      console.log('matching result', data);
      console.log('mp3 recording', data.mp3_recording);
    });
  }

  render() {
    return (
      <div className="App">
        <button onClick={this.run_matching}>Start</button>
      </div>
    );
  }
}

export default App;
