import JSController from './JSController.js';
import worker_js from './JSWorker.js';
import mp3enc_js from './mp3enc.js';
import mp3enc_mem from './mp3enc.mem';

var MatchMySound = {internal: {}};

MatchMySound.init = function() {
  return new Promise(resolve => {
    if (MatchMySound.controller) {
      resolve();
    }

    var baseUrl = get_base_url(window.location);

    window.localStore = localStorage;

    var worker_code = atob(worker_js.split(',')[1]);

    var enc_conf = "self['Mp3LameEncoderConfig']={memoryInitializerRequest:{response:Uint8Array.from(atob('MP3_MEM_DATA_INIT'),function(c){return c.charCodeAt(0)})}};";
    enc_conf = enc_conf.replace('MP3_MEM_DATA_INIT', mp3enc_mem.split(',')[1]);
    worker_code = worker_code.concat(enc_conf, atob(mp3enc_js.split(',')[1]));

    MatchMySound.internal.controller = new JSController(baseUrl, false, worker_code);

    MatchMySound.internal.init_resolve = resolve;
    MatchMySound.internal.controller.trigger = MatchMySound.internal.trigger;
  });
};

MatchMySound.load_etalon = function(url) {
  return new Promise(resolve => {
    MatchMySound.internal.etalon_resolve = resolve;
    MatchMySound.internal.controller.loadEtalon(url, 120);
  });
};

MatchMySound.start_matching = function(beg, end, sync, tempo, match_start) {
  MatchMySound.internal.chosen_tempo = tempo || 100;
  return new Promise(resolve => {
    MatchMySound.internal.match_resolve = resolve;
    new Promise(record_resolve => {
      MatchMySound.internal.record_resolve = record_resolve;
      MatchMySound.internal.controller.record();
    }).then(() => {
      if (beg) beg *= 43;
      if (end) end *= 43;
      if (match_start) match_start *= 43;
      MatchMySound.internal.controller.startMatcher(0, beg, end, true, sync, match_start, false);
      MatchMySound.internal.controller.startStorage();
      MatchMySound.internal.controller.pipeRecorder(0.0);
    })
  });
};

MatchMySound.stop_matching = function() {
  MatchMySound.internal.controller.stopCurrentAnalysis();
};

MatchMySound.internal.trigger = function(eventName, args) {
  if (eventName == 'initialize') {
    setTimeout(MatchMySound.internal.init_resolve, 100);
  }
  else if (eventName == 'HTTPRequest') {
    if (args.url.includes('sounds')) return;
    var req = new XMLHttpRequest();
    req.open(args.method, args.url);
    req.responseType = args.responseType;
    req.onload = function() {
      MatchMySound.internal.controller.submitHTTPResponse(args.url, req.response);
      MatchMySound.internal.etalon_resolve();
    }
    req.send(args.data);
  }
  else if (eventName == 'binaryDataRaw') {
    var req = new XMLHttpRequest();
    req.responseType = 'json';
    var service_url = 'https://8107ajnsx1.execute-api.us-east-1.amazonaws.com/feedback';
    var url = service_url + '?chosen_tempo=' + MatchMySound.internal.chosen_tempo;
    req.open('POST', url);
    req.onload = function() {
      var data = MatchMySound.internal.matcher_data;
      data.feedback = req.response.feedback;
      MatchMySound.internal.controller.player.getAsFile('recording', function(recdata) {
        data.mp3_recording = recdata;
        MatchMySound.internal.match_resolve(data);
      });
    }
    req.send(args.data);
  }
  else if (eventName == 'matcher.stopped') {
    MatchMySound.internal.controller.unpipeRecorder();
    MatchMySound.internal.controller.stopStorage(1.0, 'recording');
    MatchMySound.internal.controller.getMatcherData(true);
  }
  else if (eventName == 'matcherData') {
    MatchMySound.internal.matcher_data = args;
  }
  else if (eventName == 'record.start') {
    MatchMySound.internal.record_resolve();
  }
};

var get_base_url = function(url) {
  var path = url.pathname.split('/').slice(0,-1).join('/');
  return window.location.protocol + "//" + window.location.host + path + '/';
};

window.mms_script_dir = get_base_url(new URL(document.currentScript.src));

export { MatchMySound };
