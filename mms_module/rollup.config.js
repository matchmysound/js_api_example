import url from "@rollup/plugin-url";
import copy from 'rollup-plugin-copy';

const packageJson = require("./package.json");

export default [
  {
    input: "src/MatchMySound.js",
    output: [
      {
        file: packageJson.main,
        format: "cjs"
      }
    ],
    plugins: [
      url({ include: [
        "src/JSWorker.js", "src/mp3enc.js", "src/mp3enc.mem"
      ], limit: 500*1024 }),
      copy({
        targets: [{ src: 'src/types.d.ts', dest: 'dist' }]
      })
    ],
    onwarn: function(w) {
      if (w.code == 'THIS_IS_UNDEFINED') return;
      console.warn(w.message);
    }
  }
];
